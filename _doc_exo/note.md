# Projet crazy taxi



## Présentation

Un jeu de slalom en voiture sur une autoroute de plus en plus encombrée. Un format de mini-jeu très présent sur les plateformes de jeux gratuits en ligne. Le principe est très simple, nous incarnons une voiture qui doit se frayer un chemin parmi un trafic de voiture en évitant l'accident.

## Spécifications téchniques

Le jeu s'executera sur un serveur local Node.js et sera réalisé en Javascript vanilla. Pour faciliter le développement et le maintient du code celui-ci sera fractionné en plusieurs modules, l'utilisation de module Javascript est rendu possible grâce à Webpack.

Le design du jeu sera réalisé en Sass, utilisation elle aussi rendu possible grâce à Webpack.

## Développement

Bien que le cadre de ce projet ne nécessite pas d'utiliser une méthodologie de développement agile je souhaites m'appuyer sur les outils de la méthodologie agile SCRUM pour cadrer le développement de mon projet, pour cela j'utilise un tableau de suivis d'avancement (Trello).





