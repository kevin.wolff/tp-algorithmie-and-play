/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./src/sass/index.scss":
/*!*****************************!*\
  !*** ./src/sass/index.scss ***!
  \*****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n// extracted by mini-css-extract-plugin\n\n\n//# sourceURL=webpack://tp-algorithmie-and-play/./src/sass/index.scss?");

/***/ }),

/***/ "./src/js/character/Car.js":
/*!*********************************!*\
  !*** ./src/js/character/Car.js ***!
  \*********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ Car)\n/* harmony export */ });\n/* harmony import */ var _class_Vehicle_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../class/Vehicle.js */ \"./src/js/class/Vehicle.js\");\n/* harmony import */ var _function_generateVehicleDomElement_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../function/generateVehicleDomElement.js */ \"./src/js/function/generateVehicleDomElement.js\");\n\n\n\nclass Car extends _class_Vehicle_js__WEBPACK_IMPORTED_MODULE_0__.default {\n    constructor(props) {\n        super(props)\n\n        // Global and specific constants.\n        this.constGlobal = props.constGlobal\n        this.constCar = props.constVehicle\n        this.board = props.board\n        this.step = props.step\n\n        // Initializes the vehicle according to the user type.\n        this.abscissa = props.user === 'player' ? this.constGlobal.playerAbscissaStart : this.constGlobal.computerAbscissaStart[props.lane]\n        this.ordinate = props.user === 'player' ? this.constGlobal.playerOrdinateStart : this.constGlobal.computerOrdinateStart\n        this.lane = props.user === 'player' ? this.constGlobal.playerAbscissaLanes.indexOf(this.abscissa) : props.lane\n        props.user === 'player' ? this.initialization() : this.automating()\n\n        // Generate DOM element for the vehicle.\n        this.car = (0,_function_generateVehicleDomElement_js__WEBPACK_IMPORTED_MODULE_1__.default)(props.user, 'car', this.constCar, this.abscissa, this.ordinate, this.lane)\n    }\n\n    // Returns the current vehicle.\n    getVehicle() {\n        return this.car\n    }\n\n    // Initialization for human player.\n    initialization() {\n        document.addEventListener('keydown', (event) => {\n            if (this.constGlobal.keys.includes(event.key)) {\n                this.handleActions(event.key, this.car, this.step)\n                this.updatePosition(this.car)\n            }\n        })\n    }\n\n    // Initialization for computer player.\n    automating() {\n        setInterval(() => {\n            this.handleActions('auto', this.car, this.step)\n            this.updatePosition(this.car)\n        }, 60)\n    }\n}\n\n//# sourceURL=webpack://tp-algorithmie-and-play/./src/js/character/Car.js?");

/***/ }),

/***/ "./src/js/character/Police.js":
/*!************************************!*\
  !*** ./src/js/character/Police.js ***!
  \************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ Police)\n/* harmony export */ });\n/* harmony import */ var _class_Vehicle_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../class/Vehicle.js */ \"./src/js/class/Vehicle.js\");\n/* harmony import */ var _function_generateVehicleDomElement_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../function/generateVehicleDomElement.js */ \"./src/js/function/generateVehicleDomElement.js\");\n\n\n\nclass Police extends _class_Vehicle_js__WEBPACK_IMPORTED_MODULE_0__.default {\n    constructor(props) {\n        super(props)\n\n        // Global and specific constants.\n        this.constGlobal = props.constGlobal\n        this.constpolice = props.constVehicle\n        this.board = props.board\n        this.step = props.step\n\n        // Initializes the vehicle according to the user type.\n        this.abscissa = props.user === 'player' ? this.constGlobal.playerAbscissaStart : this.constGlobal.computerAbscissaStart[props.lane]\n        this.ordinate = props.user === 'player' ? this.constGlobal.playerOrdinateStart : this.constGlobal.computerOrdinateStart\n        this.lane = props.user === 'player' ? this.constGlobal.playerAbscissaLanes.indexOf(this.abscissa) : props.lane\n        props.user === 'player' ? this.initialization() : this.automating()\n\n        // Generate DOM element for the vehicle.\n        this.police = (0,_function_generateVehicleDomElement_js__WEBPACK_IMPORTED_MODULE_1__.default)(props.user, 'police', this.constpolice, this.abscissa, this.ordinate, this.lane)\n    }\n\n    // Returns the current vehicle.\n    getVehicle() {\n        return this.police\n    }\n\n    // Initialization for human player.\n    initialization() {\n        document.addEventListener('keydown', (event) => {\n            if (this.constGlobal.keys.includes(event.key)) {\n                this.handleActions(event.key, this.police, this.step)\n                this.updatePosition(this.police)\n            }\n        })\n    }\n\n    // Initialization for computer player.\n    automating() {\n        setInterval(() => {\n            this.handleActions('auto', this.police, this.step)\n            this.updatePosition(this.police)\n        }, 60)\n    }\n}\n\n//# sourceURL=webpack://tp-algorithmie-and-play/./src/js/character/Police.js?");

/***/ }),

/***/ "./src/js/character/Taxi.js":
/*!**********************************!*\
  !*** ./src/js/character/Taxi.js ***!
  \**********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ Taxi)\n/* harmony export */ });\n/* harmony import */ var _class_Vehicle_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../class/Vehicle.js */ \"./src/js/class/Vehicle.js\");\n/* harmony import */ var _function_generateVehicleDomElement_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../function/generateVehicleDomElement.js */ \"./src/js/function/generateVehicleDomElement.js\");\n\n\n\nclass Taxi extends _class_Vehicle_js__WEBPACK_IMPORTED_MODULE_0__.default {\n    constructor(props) {\n        super(props)\n\n        // Global and specific constants.\n        this.constGlobal = props.constGlobal\n        this.constTaxi = props.constVehicle\n        this.board = props.board\n        this.step = props.step\n\n        // Initializes the vehicle according to the user type.\n        this.abscissa = props.user === 'player' ? this.constGlobal.playerAbscissaStart : this.constGlobal.computerAbscissaStart[props.lane]\n        this.ordinate = props.user === 'player' ? this.constGlobal.playerOrdinateStart : this.constGlobal.computerOrdinateStart\n        this.lane = props.user === 'player' ? this.constGlobal.playerAbscissaLanes.indexOf(this.abscissa) : props.lane\n        props.user === 'player' ? this.initialization() : this.automating()\n\n        // Generate DOM element for the vehicle.\n        this.taxi = (0,_function_generateVehicleDomElement_js__WEBPACK_IMPORTED_MODULE_1__.default)(props.user,'taxi', this.constTaxi, this.abscissa, this.ordinate, this.lane)\n    }\n\n    // Returns the current vehicle.\n    getVehicle() {\n        return this.taxi\n    }\n\n    // Initialization for human player.\n    initialization() {\n        document.addEventListener('keydown', (event) => {\n            if (this.constGlobal.keys.includes(event.key)) {\n                this.handleActions(event.key, this.taxi, this.step)\n                this.updatePosition(this.taxi)\n            }\n        })\n    }\n\n    // Initialization for computer player.\n    automating() {\n        setInterval(() => {\n            this.handleActions('auto', this.taxi, this.step)\n            this.updatePosition(this.taxi)\n        }, 60)\n    }\n}\n\n//# sourceURL=webpack://tp-algorithmie-and-play/./src/js/character/Taxi.js?");

/***/ }),

/***/ "./src/js/character/Truck.js":
/*!***********************************!*\
  !*** ./src/js/character/Truck.js ***!
  \***********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ Truck)\n/* harmony export */ });\n/* harmony import */ var _class_Vehicle_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../class/Vehicle.js */ \"./src/js/class/Vehicle.js\");\n/* harmony import */ var _function_generateVehicleDomElement_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../function/generateVehicleDomElement.js */ \"./src/js/function/generateVehicleDomElement.js\");\n\n\n\nclass Truck extends _class_Vehicle_js__WEBPACK_IMPORTED_MODULE_0__.default {\n    constructor(props) {\n        super(props)\n\n        // Global and specific constants.\n        this.constGlobal = props.constGlobal\n        this.constTruck = props.constVehicle\n        this.board = props.board\n        this.step = props.step\n\n        // Initializes the vehicle according to the user type.\n        this.abscissa = props.user === 'player' ? this.constGlobal.playerAbscissaStart : this.constGlobal.computerAbscissaStart[props.lane]\n        this.ordinate = props.user === 'player' ? this.constGlobal.playerOrdinateStart : this.constGlobal.computerOrdinateStart\n        this.lane = props.user === 'player' ? this.constGlobal.playerAbscissaLanes.indexOf(this.abscissa) : props.lane\n        props.user === 'player' ? this.initialization() : this.automating()\n\n        // Generate DOM element for the vehicle.\n        this.truck = (0,_function_generateVehicleDomElement_js__WEBPACK_IMPORTED_MODULE_1__.default)(props.user,'truck', this.constTruck, this.abscissa, this.ordinate, this.lane)\n    }\n\n    // Returns the current vehicle.\n    getVehicle() {\n        return this.truck\n    }\n\n    // Initialization for human player.\n    initialization() {\n        document.addEventListener('keydown', (event) => {\n            if (this.constGlobal.keys.includes(event.key)) {\n                this.handleActions(event.key, this.truck, this.step)\n                this.updatePosition(this.truck)\n            }\n        })\n    }\n\n    // Initialization for computer player.\n    automating() {\n        setInterval(() => {\n            this.handleActions('auto', this.truck, this.step)\n            this.updatePosition(this.truck)\n        }, 60)\n    }\n}\n\n//# sourceURL=webpack://tp-algorithmie-and-play/./src/js/character/Truck.js?");

/***/ }),

/***/ "./src/js/class/Proxy.js":
/*!*******************************!*\
  !*** ./src/js/class/Proxy.js ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ Proxy)\n/* harmony export */ });\n/* harmony import */ var _character_Taxi_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../character/Taxi.js */ \"./src/js/character/Taxi.js\");\n/* harmony import */ var _character_Car_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../character/Car.js */ \"./src/js/character/Car.js\");\n/* harmony import */ var _character_Truck_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../character/Truck.js */ \"./src/js/character/Truck.js\");\n/* harmony import */ var _character_Police_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../character/Police.js */ \"./src/js/character/Police.js\");\n\n\n\n\n\nclass Proxy {\n    constructor(className, options) {\n        const vehicleClasses = {\n            Taxi: _character_Taxi_js__WEBPACK_IMPORTED_MODULE_0__.default,\n            Car: _character_Car_js__WEBPACK_IMPORTED_MODULE_1__.default,\n            Truck: _character_Truck_js__WEBPACK_IMPORTED_MODULE_2__.default,\n            Police: _character_Police_js__WEBPACK_IMPORTED_MODULE_3__.default\n        }\n\n        return new vehicleClasses[className](options)\n    }\n}\n\n//# sourceURL=webpack://tp-algorithmie-and-play/./src/js/class/Proxy.js?");

/***/ }),

/***/ "./src/js/class/Vehicle.js":
/*!*********************************!*\
  !*** ./src/js/class/Vehicle.js ***!
  \*********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ Vehicle)\n/* harmony export */ });\n/* harmony import */ var _function_isThereAvailableLane_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../function/isThereAvailableLane.js */ \"./src/js/function/isThereAvailableLane.js\");\n/* harmony import */ var _function_isThereObstacles_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../function/isThereObstacles.js */ \"./src/js/function/isThereObstacles.js\");\n/* harmony import */ var _function_isThereWalls_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../function/isThereWalls.js */ \"./src/js/function/isThereWalls.js\");\n\n\n\n\nclass Vehicle {\n    constructor(props) {\n\n        this.constVehicle = props.constVehicle\n        this.board = props.board\n        this.availablePlayerVehicleLanes = props.constGlobal.playerAbscissaLanes\n        this.direction = ''\n    }\n\n    // Manage the actions according to the key pressed.\n    handleActions(key, currentVehicle, step) {\n        this.direction = key\n        const currentPlayerVehicleLane = this.availablePlayerVehicleLanes.indexOf(this.abscissa)\n\n        switch (key) {\n            case 'q':\n                if ((0,_function_isThereAvailableLane_js__WEBPACK_IMPORTED_MODULE_0__.default)(currentPlayerVehicleLane, this.direction)) {\n                    this.abscissa = this.availablePlayerVehicleLanes[currentPlayerVehicleLane - 1]\n                    currentVehicle.dataset.lane = this.availablePlayerVehicleLanes.indexOf(this.abscissa)\n                }\n                break\n            case 'd':\n                if ((0,_function_isThereAvailableLane_js__WEBPACK_IMPORTED_MODULE_0__.default)(currentPlayerVehicleLane, this.direction)) {\n                    this.abscissa = this.availablePlayerVehicleLanes[currentPlayerVehicleLane + 1]\n                    currentVehicle.dataset.lane = this.availablePlayerVehicleLanes.indexOf(this.abscissa)\n                }\n                break\n            case 'auto':\n                if (!(0,_function_isThereWalls_js__WEBPACK_IMPORTED_MODULE_2__.default)(currentVehicle, this.board.style.height, this.constVehicle.height) &&\n                    !(0,_function_isThereObstacles_js__WEBPACK_IMPORTED_MODULE_1__.default)(currentVehicle)) {\n                    this.ordinate = this.ordinate + step\n                }\n                break\n        }\n    }\n\n    // Updates the current position of the object.\n    updatePosition(currentVehicle) {\n        if (this.direction === 'q' || this.direction === 'd') {\n            currentVehicle.style.left = `${this.abscissa - (this.constVehicle.width / 2)}px`\n        }\n        if (this.direction === 'auto') {\n            currentVehicle.style.top = `${this.ordinate - (this.constVehicle.height / 2)}px`\n        }\n    }\n}\n\n//# sourceURL=webpack://tp-algorithmie-and-play/./src/js/class/Vehicle.js?");

/***/ }),

/***/ "./src/js/dialogue/gameoverDialogueGenerator.js":
/*!******************************************************!*\
  !*** ./src/js/dialogue/gameoverDialogueGenerator.js ***!
  \******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ gameoverDialogueGenerator)\n/* harmony export */ });\n/* harmony import */ var _index_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../index.js */ \"./src/js/index.js\");\n\n\n// Generates the game over dialogue.\nfunction gameoverDialogueGenerator() {\n\n    // Cleans computer vehicle generator interval.\n    clearInterval(_index_js__WEBPACK_IMPORTED_MODULE_0__.intervalGeneratorVehicle)\n    clearInterval(_index_js__WEBPACK_IMPORTED_MODULE_0__.intervalScoreUpdate)\n\n    // Cleans computer vehicle still present on board.\n    const currentComputerVehicles = document.querySelectorAll('.computer')\n    currentComputerVehicles.forEach(vehicle => {\n        vehicle.remove()\n    })\n\n    const root = document.querySelector('#root')\n\n    const popup = document.createElement('div')\n    popup.setAttribute('class', 'popup')\n    const msg = document.createElement('p')\n    msg.innerText = 'Game over'\n    const score = document.createElement('p')\n    score.innerText = document.querySelector('.score-value > p').innerHTML\n    const btn = document.createElement('div')\n    btn.setAttribute('class', 'button')\n    btn.innerText = 'Play again'\n\n    btn.addEventListener('click', () => {\n        document.location.reload()\n    })\n\n    // Appends DOM elements.\n    popup.append(msg, score, btn)\n    root.append(popup)\n}\n\n//# sourceURL=webpack://tp-algorithmie-and-play/./src/js/dialogue/gameoverDialogueGenerator.js?");

/***/ }),

/***/ "./src/js/dialogue/startDialogueGenerator.js":
/*!***************************************************!*\
  !*** ./src/js/dialogue/startDialogueGenerator.js ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ startDialogueGenerator)\n/* harmony export */ });\n// Generates the launcher dialogue.\nfunction startDialogueGenerator(availableVehicles, availableDifficulty) {\n\n    // Main div element.\n    const popup = document.createElement('div')\n    popup.setAttribute('class', 'popup')\n\n    // Form element.\n    const form = document.createElement('form')\n    form.setAttribute('class', 'form')\n\n    // Select vehicle element.\n    const labelSelectVehicle = document.createElement('label')\n    labelSelectVehicle.setAttribute('for', 'selectVehicle')\n    labelSelectVehicle.innerText = 'Select vehicle'\n    const selectVehicle = document.createElement('select')\n    selectVehicle.setAttribute('id', 'selectVehicle')\n    selectVehicle.setAttribute('class', 'select')\n    availableVehicles.forEach(vehicleName => {\n        const option = document.createElement('option')\n        option.value = vehicleName\n        option.text = vehicleName\n\n        selectVehicle.add(option)\n    })\n\n    // Select difficulty element.\n    const labelSelectDifficulty = document.createElement('label')\n    labelSelectDifficulty.setAttribute('for', 'selectDifficulty')\n    labelSelectDifficulty.innerText = 'Select difficulty'\n    const selectDifficulty = document.createElement('select')\n    selectDifficulty.setAttribute('id', 'selectDifficulty')\n    selectDifficulty.setAttribute('class', 'select')\n    for (let difficulty in availableDifficulty) {\n        const option = document.createElement('option')\n        option.value = availableDifficulty[`${difficulty}`]\n        option.text = difficulty\n\n        selectDifficulty.add(option)\n    }\n\n    // Submit form element.\n    const submit = document.createElement('div')\n    submit.setAttribute('class', 'button')\n    submit.innerText = 'Let\\'s go'\n\n    // Appends DOM elements.\n    form.append(labelSelectVehicle, selectVehicle, labelSelectDifficulty, selectDifficulty, submit)\n    popup.append(form)\n\n    return popup\n}\n\n//# sourceURL=webpack://tp-algorithmie-and-play/./src/js/dialogue/startDialogueGenerator.js?");

/***/ }),

/***/ "./src/js/environment/Board.js":
/*!*************************************!*\
  !*** ./src/js/environment/Board.js ***!
  \*************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ board)\n/* harmony export */ });\nclass board {\n    constructor() {\n    }\n\n    // Generates game board interface.\n    static generateBoardInterface(width, height) {\n        const numLane = 3\n        const boardInterface = document.createElement('div')\n        boardInterface.setAttribute('class', 'board')\n        boardInterface.style.width = `${width}px`\n        boardInterface.style.height = `${height}px`\n        boardInterface.style.position = 'relative'\n        boardInterface.style.display = 'flex'\n\n        for (let i = 0; i < numLane; i++) {\n            let lane = document.createElement('div')\n            lane.setAttribute('class', `lane lane-${i}`)\n\n            lane.style.width = `${(width / 3)}px`\n            lane.style.height = `${height}px`\n            boardInterface.append(lane)\n        }\n\n        return boardInterface\n    }\n}\n\n//# sourceURL=webpack://tp-algorithmie-and-play/./src/js/environment/Board.js?");

/***/ }),

/***/ "./src/js/environment/Score.js":
/*!*************************************!*\
  !*** ./src/js/environment/Score.js ***!
  \*************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ Score)\n/* harmony export */ });\nclass Score {\n    constructor() {\n    }\n\n    // Generates the score interface.\n    static generateScoreInterface() {\n        const scoreInterface = document.createElement('div')\n        scoreInterface.setAttribute('class', 'score-interface')\n\n        const scoreValue = document.createElement('div')\n        scoreValue.setAttribute('class', 'score-value')\n\n        const score = document.createElement('p')\n        score.innerHTML = '0'\n\n        scoreValue.append(score)\n        scoreInterface.append(scoreValue)\n\n        return scoreInterface\n    }\n}\n\n\n//# sourceURL=webpack://tp-algorithmie-and-play/./src/js/environment/Score.js?");

/***/ }),

/***/ "./src/js/function/checkLastVehicleDistance.js":
/*!*****************************************************!*\
  !*** ./src/js/function/checkLastVehicleDistance.js ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ checkLastVehicleDistance)\n/* harmony export */ });\n// Check the distance between the next and the last vehicle.\nfunction checkLastVehicleDistance(playerVehicleHeight) {\n\n    const currentCars = document.querySelectorAll(`.computer`)\n    const closestCar = currentCars[currentCars.length -1]\n\n    return closestCar.offsetTop >= (playerVehicleHeight * 3);\n}\n\n//# sourceURL=webpack://tp-algorithmie-and-play/./src/js/function/checkLastVehicleDistance.js?");

/***/ }),

/***/ "./src/js/function/generateVehicleDomElement.js":
/*!******************************************************!*\
  !*** ./src/js/function/generateVehicleDomElement.js ***!
  \******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ generateVehicleDomElement)\n/* harmony export */ });\n// Generates the vehicle DOM element.\nfunction generateVehicleDomElement(user, typeVehicle, constVehicle, abscissa, ordinate, lane) {\n\n    const vehicle = document.createElement('div')\n    vehicle.setAttribute('class', `${user} ${typeVehicle}`)\n    vehicle.dataset.lane = lane\n    vehicle.style.position = 'absolute'\n    vehicle.style.left = (`${abscissa - (constVehicle.width / 2)}px`)\n    vehicle.style.top = (`${ordinate - (constVehicle.height / 2)}px`)\n    vehicle.style.width = `${constVehicle.width}px`\n    vehicle.style.height = `${constVehicle.height}px`\n    vehicle.style.backgroundColor = `${constVehicle.color}`\n\n    return vehicle\n}\n\n//# sourceURL=webpack://tp-algorithmie-and-play/./src/js/function/generateVehicleDomElement.js?");

/***/ }),

/***/ "./src/js/function/getAvailableVehicles.js":
/*!*************************************************!*\
  !*** ./src/js/function/getAvailableVehicles.js ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ getAvailableVehicles)\n/* harmony export */ });\n/* harmony import */ var _util_constant_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../util/constant.js */ \"./src/js/util/constant.js\");\n\n\n// Returns all available vehicle present in constant.vehicles.\nfunction getAvailableVehicles() {\n\n    const result = []\n    const enableVehicles = _util_constant_js__WEBPACK_IMPORTED_MODULE_0__.vehicles\n    for (let vehicle in enableVehicles) {\n        result.push(vehicle.charAt(0).toUpperCase() + vehicle.slice(1))\n    }\n\n    return result\n}\n\n//# sourceURL=webpack://tp-algorithmie-and-play/./src/js/function/getAvailableVehicles.js?");

/***/ }),

/***/ "./src/js/function/isThereAvailableLane.js":
/*!*************************************************!*\
  !*** ./src/js/function/isThereAvailableLane.js ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ isThereAvailableLane)\n/* harmony export */ });\n// Checks if there is an available lane.\nfunction isThereAvailableLane(currentPlayerAbscissaLane, direction) {\n\n    return direction === 'q' && currentPlayerAbscissaLane > 0 ||\n        direction === 'd' && currentPlayerAbscissaLane < 2;\n}\n\n//# sourceURL=webpack://tp-algorithmie-and-play/./src/js/function/isThereAvailableLane.js?");

/***/ }),

/***/ "./src/js/function/isThereObstacles.js":
/*!*********************************************!*\
  !*** ./src/js/function/isThereObstacles.js ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ isThereObstacles)\n/* harmony export */ });\n/* harmony import */ var _dialogue_gameoverDialogueGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../dialogue/gameoverDialogueGenerator.js */ \"./src/js/dialogue/gameoverDialogueGenerator.js\");\n\n\n// Checks if there are any obstacles.\nfunction isThereObstacles(currentVehicle) {\n\n    const playerVehicle = document.querySelector('.player')\n    const playerVehicleOffsetTop = playerVehicle.getBoundingClientRect().top\n    const playerVehicleCurrentLane = playerVehicle.dataset.lane\n\n    const computerVehicle = currentVehicle\n    const computerVehicleOffsetBottom = computerVehicle.getBoundingClientRect().bottom\n    const computerVehicleCurrentLane = computerVehicle.dataset.lane\n\n    if ((computerVehicleOffsetBottom - playerVehicleOffsetTop) > 0\n        && playerVehicleCurrentLane === computerVehicleCurrentLane) {\n\n        (0,_dialogue_gameoverDialogueGenerator_js__WEBPACK_IMPORTED_MODULE_0__.default)()\n        return true\n    }\n\n    return false\n}\n\n//# sourceURL=webpack://tp-algorithmie-and-play/./src/js/function/isThereObstacles.js?");

/***/ }),

/***/ "./src/js/function/isThereWalls.js":
/*!*****************************************!*\
  !*** ./src/js/function/isThereWalls.js ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ isThereWalls)\n/* harmony export */ });\n// Checks if there are any walls.\nfunction isThereWalls(currentVehicle, boardHeight, vehicleHeight) {\n\n    const offsetBottom = parseInt(boardHeight.replace('px', '')) - (currentVehicle.offsetTop + vehicleHeight)\n    if (offsetBottom < 20) {\n        currentVehicle.remove()\n    }\n\n    return false\n}\n\n//# sourceURL=webpack://tp-algorithmie-and-play/./src/js/function/isThereWalls.js?");

/***/ }),

/***/ "./src/js/index.js":
/*!*************************!*\
  !*** ./src/js/index.js ***!
  \*************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"intervalGeneratorVehicle\": () => (/* binding */ intervalGeneratorVehicle),\n/* harmony export */   \"intervalScoreUpdate\": () => (/* binding */ intervalScoreUpdate)\n/* harmony export */ });\n/* harmony import */ var _util_constant_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./util/constant.js */ \"./src/js/util/constant.js\");\n/* harmony import */ var _class_Proxy_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./class/Proxy.js */ \"./src/js/class/Proxy.js\");\n/* harmony import */ var _environment_Board_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./environment/Board.js */ \"./src/js/environment/Board.js\");\n/* harmony import */ var _environment_Score_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environment/Score.js */ \"./src/js/environment/Score.js\");\n/* harmony import */ var _dialogue_startDialogueGenerator_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./dialogue/startDialogueGenerator.js */ \"./src/js/dialogue/startDialogueGenerator.js\");\n/* harmony import */ var _function_getAvailableVehicles_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./function/getAvailableVehicles.js */ \"./src/js/function/getAvailableVehicles.js\");\n/* harmony import */ var _function_checkLastVehicleDistance_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./function/checkLastVehicleDistance.js */ \"./src/js/function/checkLastVehicleDistance.js\");\n/* harmony import */ var _sass_index_scss__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../sass/index.scss */ \"./src/sass/index.scss\");\n\n\n\n\n\n\n\n\n\nconst root = document.querySelector('#root')\nconst availableVehicles = (0,_function_getAvailableVehicles_js__WEBPACK_IMPORTED_MODULE_5__.default)()\nconst availableDifficulty = _util_constant_js__WEBPACK_IMPORTED_MODULE_0__.constants.difficulty\nlet intervalGeneratorVehicle\nlet intervalScoreUpdate\nlet currentPlayerScore = 0\n\n// Launcher dialogue.\nconst startDialogue = (0,_dialogue_startDialogueGenerator_js__WEBPACK_IMPORTED_MODULE_4__.default)(availableVehicles, availableDifficulty)\nroot.appendChild(startDialogue)\n\n// Launcher dialogue's submit button function.\nconst submit = document.querySelector('.button')\nsubmit.addEventListener('click', () => {\n    let playerVehicleChoice = ''\n    let playerDifficultyChoice = ''\n\n    const selectInputs = document.querySelectorAll('.select')\n    selectInputs.forEach(select => {\n        switch (select.attributes.id.value) {\n            case 'selectVehicle':\n                playerVehicleChoice = select.value\n                break\n            case 'selectDifficulty':\n                playerDifficultyChoice = parseInt(select.value)\n                break\n        }\n    })\n\n    startDialogue.remove()\n    startGame(playerVehicleChoice, playerDifficultyChoice)\n})\n\n// Game life cycle.\nfunction startGame(playerVehicleName, playerDifficultyValue) {\n\n    // Generates game board.\n    const board = _environment_Board_js__WEBPACK_IMPORTED_MODULE_2__.default.generateBoardInterface(_util_constant_js__WEBPACK_IMPORTED_MODULE_0__.board.width, _util_constant_js__WEBPACK_IMPORTED_MODULE_0__.board.height)\n\n    // Generates game score.\n    const score = _environment_Score_js__WEBPACK_IMPORTED_MODULE_3__.default.generateScoreInterface()\n\n    // Start score counting.\n    intervalScoreUpdate = setInterval(() => {\n        currentPlayerScore = currentPlayerScore + (10 * playerDifficultyValue)\n        const scoreValue = document.querySelector('.score-value > p')\n        scoreValue.innerHTML = currentPlayerScore\n    },1000)\n\n    // Initializes the player's vehicle class.\n    const playerVehicle = new _class_Proxy_js__WEBPACK_IMPORTED_MODULE_1__.default(playerVehicleName, {\n        user: 'player',\n        constGlobal: _util_constant_js__WEBPACK_IMPORTED_MODULE_0__.constants,\n        constVehicle: _util_constant_js__WEBPACK_IMPORTED_MODULE_0__.vehicles[`${playerVehicleName.charAt(0).toLowerCase() + playerVehicleName.slice(1)}`],\n        board: board,\n        step: null,\n        lane: null,\n    })\n\n    // Initializes a first class of random vehicle.\n    let randomVehicleName = availableVehicles[Math.floor(Math.random() * availableVehicles.length)]\n    const randomVehicle = new _class_Proxy_js__WEBPACK_IMPORTED_MODULE_1__.default(randomVehicleName, {\n        user: 'computer',\n        constGlobal: _util_constant_js__WEBPACK_IMPORTED_MODULE_0__.constants,\n        constVehicle: _util_constant_js__WEBPACK_IMPORTED_MODULE_0__.vehicles[`${randomVehicleName.charAt(0).toLowerCase() + randomVehicleName.slice(1)}`],\n        board: board,\n        step: playerDifficultyValue,\n        lane: Math.floor(Math.random() * 3)\n    }).getVehicle()\n\n    // Vehicle creation interval.\n    intervalGeneratorVehicle = setInterval(() => {\n        if ((0,_function_checkLastVehicleDistance_js__WEBPACK_IMPORTED_MODULE_6__.default)(playerVehicle.constVehicle.height)) {\n\n            // Initializes a class of random vehicle.\n            randomVehicleName = availableVehicles[Math.floor(Math.random() * availableVehicles.length)]\n            const randomVehicle = new _class_Proxy_js__WEBPACK_IMPORTED_MODULE_1__.default(randomVehicleName, {\n                user: 'computer',\n                constGlobal: _util_constant_js__WEBPACK_IMPORTED_MODULE_0__.constants,\n                constVehicle: _util_constant_js__WEBPACK_IMPORTED_MODULE_0__.vehicles[`${randomVehicleName.charAt(0).toLowerCase() + randomVehicleName.slice(1)}`],\n                board: board,\n                step: playerDifficultyValue,\n                lane: Math.floor(Math.random() * 2.5)\n            }).getVehicle()\n\n            board.appendChild(randomVehicle)\n        }\n    }, 60)\n\n    // Appends DOM elements.\n    board.appendChild(playerVehicle.getVehicle())\n    board.appendChild(randomVehicle)\n    root.append(score, board)\n}\n\n\n\n//# sourceURL=webpack://tp-algorithmie-and-play/./src/js/index.js?");

/***/ }),

/***/ "./src/js/util/constant.js":
/*!*********************************!*\
  !*** ./src/js/util/constant.js ***!
  \*********************************/
/***/ ((__unused_webpack_module, exports) => {

eval("// Global constants.\nexports.constants = {\n    keys: ['q', 'd'],\n    playerAbscissaStart: 150,\n    playerOrdinateStart: 500,\n    playerAbscissaLanes: [50, 150, 250],\n    computerAbscissaStart: [50, 150, 250],\n    computerOrdinateStart: 50,\n    difficulty: {slow: 3, normal: 5, fun: 7, intermediate: 10, speed: 20, god: 40}\n}\n\n// Board game constants.\nexports.board = {\n    width: 300,\n    height: 550,\n}\n\n// Vehicles constants.\nexports.vehicles = {\n    taxi: {\n        type: 'taxi',\n        width: 40,\n        height: 55,\n    },\n    car: {\n        type: 'car',\n        width: 40,\n        height: 40,\n    },\n    truck: {\n        type: 'truck',\n        width: 40,\n        height: 90,\n    },\n    police: {\n        type: 'police',\n        width: 50,\n        height: 50,\n    },\n}\n\n//# sourceURL=webpack://tp-algorithmie-and-play/./src/js/util/constant.js?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module is referenced by other modules so it can't be inlined
/******/ 	var __webpack_exports__ = __webpack_require__("./src/js/index.js");
/******/ 	
/******/ })()
;