# TP algorithmie

## Consignes

Content de votre travail sur le bundling de Pacman effectué précédemment, votre entreprise vous propose de développer un jeu vidéo 2D rétro comme Donkey-Kong, Mario, etc. Ou si vous avez une idée originale, de la développer, pour peu que le jeu ai un look retro et un intérêt réel pour le joueur. Le langage conseillé par simplicité et le  javascript/typescript avec Node, via affichage sur un navigateur, mais  l'utilisation d'un langage POO avec une librarie qui permet de gérer des graphismes (comme Java) peut-être envisagé si vous le souhaitez.

Vous disposerez de 5 jours, qui pourront s'organiser en différentes périodes :

• choix et définition du projet. Mise à  plat des réflections, création des diagrammes UML sur la structure du  jeu, et de la construction du projet (diagramme objet), relevé des  contraintes et solutions pour les résoudre.

• Développement des composants du jeu.

• Tests et debug.

### Livrables

**Fichiers disponibles dans** `_doc_exo/`

- notes du projet
- diagram du cycle de vie du jeu

## Réalisation

### Initialiser le projet

```shell
npm install
npx webpack --watch
```

Projet initialisé avec [CreateMyProject.sh](https://gitlab.com/kevin.wolff/create-my-project.sh) (projet personnel).

