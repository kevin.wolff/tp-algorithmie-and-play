import Vehicle from '../class/Vehicle.js'
import generateVehicleDomElement from '../function/generateVehicleDomElement.js'

export default class Car extends Vehicle {
    constructor(props) {
        super(props)

        // Global and specific constants.
        this.constGlobal = props.constGlobal
        this.constCar = props.constVehicle
        this.board = props.board
        this.step = props.step

        // Initializes the vehicle according to the user type.
        this.abscissa = props.user === 'player' ? this.constGlobal.playerAbscissaStart : this.constGlobal.computerAbscissaStart[props.lane]
        this.ordinate = props.user === 'player' ? this.constGlobal.playerOrdinateStart : this.constGlobal.computerOrdinateStart
        this.lane = props.user === 'player' ? this.constGlobal.playerAbscissaLanes.indexOf(this.abscissa) : props.lane
        props.user === 'player' ? this.initialization() : this.automating()

        // Generate DOM element for the vehicle.
        this.car = generateVehicleDomElement(props.user, 'car', this.constCar, this.abscissa, this.ordinate, this.lane)
    }

    // Returns the current vehicle.
    getVehicle() {
        return this.car
    }

    // Initialization for human player.
    initialization() {
        document.addEventListener('keydown', (event) => {
            if (this.constGlobal.keys.includes(event.key)) {
                this.handleActions(event.key, this.car, this.step)
                this.updatePosition(this.car)
            }
        })
    }

    // Initialization for computer player.
    automating() {
        setInterval(() => {
            this.handleActions('auto', this.car, this.step)
            this.updatePosition(this.car)
        }, 60)
    }
}