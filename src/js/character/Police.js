import Vehicle from '../class/Vehicle.js'
import generateVehicleDomElement from '../function/generateVehicleDomElement.js'

export default class Police extends Vehicle {
    constructor(props) {
        super(props)

        // Global and specific constants.
        this.constGlobal = props.constGlobal
        this.constpolice = props.constVehicle
        this.board = props.board
        this.step = props.step

        // Initializes the vehicle according to the user type.
        this.abscissa = props.user === 'player' ? this.constGlobal.playerAbscissaStart : this.constGlobal.computerAbscissaStart[props.lane]
        this.ordinate = props.user === 'player' ? this.constGlobal.playerOrdinateStart : this.constGlobal.computerOrdinateStart
        this.lane = props.user === 'player' ? this.constGlobal.playerAbscissaLanes.indexOf(this.abscissa) : props.lane
        props.user === 'player' ? this.initialization() : this.automating()

        // Generate DOM element for the vehicle.
        this.police = generateVehicleDomElement(props.user, 'police', this.constpolice, this.abscissa, this.ordinate, this.lane)
    }

    // Returns the current vehicle.
    getVehicle() {
        return this.police
    }

    // Initialization for human player.
    initialization() {
        document.addEventListener('keydown', (event) => {
            if (this.constGlobal.keys.includes(event.key)) {
                this.handleActions(event.key, this.police, this.step)
                this.updatePosition(this.police)
            }
        })
    }

    // Initialization for computer player.
    automating() {
        setInterval(() => {
            this.handleActions('auto', this.police, this.step)
            this.updatePosition(this.police)
        }, 60)
    }
}