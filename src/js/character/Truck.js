import Vehicle from '../class/Vehicle.js'
import generateVehicleDomElement from '../function/generateVehicleDomElement.js'

export default class Truck extends Vehicle {
    constructor(props) {
        super(props)

        // Global and specific constants.
        this.constGlobal = props.constGlobal
        this.constTruck = props.constVehicle
        this.board = props.board
        this.step = props.step

        // Initializes the vehicle according to the user type.
        this.abscissa = props.user === 'player' ? this.constGlobal.playerAbscissaStart : this.constGlobal.computerAbscissaStart[props.lane]
        this.ordinate = props.user === 'player' ? this.constGlobal.playerOrdinateStart : this.constGlobal.computerOrdinateStart
        this.lane = props.user === 'player' ? this.constGlobal.playerAbscissaLanes.indexOf(this.abscissa) : props.lane
        props.user === 'player' ? this.initialization() : this.automating()

        // Generate DOM element for the vehicle.
        this.truck = generateVehicleDomElement(props.user,'truck', this.constTruck, this.abscissa, this.ordinate, this.lane)
    }

    // Returns the current vehicle.
    getVehicle() {
        return this.truck
    }

    // Initialization for human player.
    initialization() {
        document.addEventListener('keydown', (event) => {
            if (this.constGlobal.keys.includes(event.key)) {
                this.handleActions(event.key, this.truck, this.step)
                this.updatePosition(this.truck)
            }
        })
    }

    // Initialization for computer player.
    automating() {
        setInterval(() => {
            this.handleActions('auto', this.truck, this.step)
            this.updatePosition(this.truck)
        }, 60)
    }
}