import Vehicle from '../class/Vehicle.js'
import generateVehicleDomElement from '../function/generateVehicleDomElement.js'

export default class Taxi extends Vehicle {
    constructor(props) {
        super(props)

        // Global and specific constants.
        this.constGlobal = props.constGlobal
        this.constTaxi = props.constVehicle
        this.board = props.board
        this.step = props.step

        // Initializes the vehicle according to the user type.
        this.abscissa = props.user === 'player' ? this.constGlobal.playerAbscissaStart : this.constGlobal.computerAbscissaStart[props.lane]
        this.ordinate = props.user === 'player' ? this.constGlobal.playerOrdinateStart : this.constGlobal.computerOrdinateStart
        this.lane = props.user === 'player' ? this.constGlobal.playerAbscissaLanes.indexOf(this.abscissa) : props.lane
        props.user === 'player' ? this.initialization() : this.automating()

        // Generate DOM element for the vehicle.
        this.taxi = generateVehicleDomElement(props.user,'taxi', this.constTaxi, this.abscissa, this.ordinate, this.lane)
    }

    // Returns the current vehicle.
    getVehicle() {
        return this.taxi
    }

    // Initialization for human player.
    initialization() {
        document.addEventListener('keydown', (event) => {
            if (this.constGlobal.keys.includes(event.key)) {
                this.handleActions(event.key, this.taxi, this.step)
                this.updatePosition(this.taxi)
            }
        })
    }

    // Initialization for computer player.
    automating() {
        setInterval(() => {
            this.handleActions('auto', this.taxi, this.step)
            this.updatePosition(this.taxi)
        }, 60)
    }
}