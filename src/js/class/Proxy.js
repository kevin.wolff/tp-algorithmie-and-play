import Taxi from '../character/Taxi.js'
import Car from '../character/Car.js'
import Truck from '../character/Truck.js'
import Police from '../character/Police.js'

export default class Proxy {
    constructor(className, options) {
        const vehicleClasses = {
            Taxi,
            Car,
            Truck,
            Police
        }

        return new vehicleClasses[className](options)
    }
}