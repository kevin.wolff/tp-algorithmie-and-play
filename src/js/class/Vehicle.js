import isThereAvailableLane from '../function/isThereAvailableLane.js'
import isThereObstacles from '../function/isThereObstacles.js'
import isThereWalls from '../function/isThereWalls.js'

export default class Vehicle {
    constructor(props) {

        this.constVehicle = props.constVehicle
        this.board = props.board
        this.availablePlayerVehicleLanes = props.constGlobal.playerAbscissaLanes
        this.direction = ''
    }

    // Manage the actions according to the key pressed.
    handleActions(key, currentVehicle, step) {
        this.direction = key
        const currentPlayerVehicleLane = this.availablePlayerVehicleLanes.indexOf(this.abscissa)

        switch (key) {
            case 'q':
                if (isThereAvailableLane(currentPlayerVehicleLane, this.direction)) {
                    this.abscissa = this.availablePlayerVehicleLanes[currentPlayerVehicleLane - 1]
                    currentVehicle.dataset.lane = this.availablePlayerVehicleLanes.indexOf(this.abscissa)
                }
                break
            case 'd':
                if (isThereAvailableLane(currentPlayerVehicleLane, this.direction)) {
                    this.abscissa = this.availablePlayerVehicleLanes[currentPlayerVehicleLane + 1]
                    currentVehicle.dataset.lane = this.availablePlayerVehicleLanes.indexOf(this.abscissa)
                }
                break
            case 'auto':
                if (!isThereWalls(currentVehicle, this.board.style.height, this.constVehicle.height) &&
                    !isThereObstacles(currentVehicle)) {
                    this.ordinate = this.ordinate + step
                }
                break
        }
    }

    // Updates the current position of the object.
    updatePosition(currentVehicle) {
        if (this.direction === 'q' || this.direction === 'd') {
            currentVehicle.style.left = `${this.abscissa - (this.constVehicle.width / 2)}px`
        }
        if (this.direction === 'auto') {
            currentVehicle.style.top = `${this.ordinate - (this.constVehicle.height / 2)}px`
        }
    }
}