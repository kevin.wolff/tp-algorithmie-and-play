// Generates the launcher dialogue.
export default function startDialogueGenerator(availableVehicles, availableDifficulty) {

    // Main div element.
    const popup = document.createElement('div')
    popup.setAttribute('class', 'popup')

    // Form element.
    const form = document.createElement('form')
    form.setAttribute('class', 'form')

    // Select vehicle element.
    const labelSelectVehicle = document.createElement('label')
    labelSelectVehicle.setAttribute('for', 'selectVehicle')
    labelSelectVehicle.innerText = 'Select vehicle'
    const selectVehicle = document.createElement('select')
    selectVehicle.setAttribute('id', 'selectVehicle')
    selectVehicle.setAttribute('class', 'select')
    availableVehicles.forEach(vehicleName => {
        const option = document.createElement('option')
        option.value = vehicleName
        option.text = vehicleName

        selectVehicle.add(option)
    })

    // Select difficulty element.
    const labelSelectDifficulty = document.createElement('label')
    labelSelectDifficulty.setAttribute('for', 'selectDifficulty')
    labelSelectDifficulty.innerText = 'Select difficulty'
    const selectDifficulty = document.createElement('select')
    selectDifficulty.setAttribute('id', 'selectDifficulty')
    selectDifficulty.setAttribute('class', 'select')
    for (let difficulty in availableDifficulty) {
        const option = document.createElement('option')
        option.value = availableDifficulty[`${difficulty}`]
        option.text = difficulty

        selectDifficulty.add(option)
    }

    // Submit form element.
    const submit = document.createElement('div')
    submit.setAttribute('class', 'button')
    submit.innerText = 'Let\'s go'

    // Appends DOM elements.
    form.append(labelSelectVehicle, selectVehicle, labelSelectDifficulty, selectDifficulty, submit)
    popup.append(form)

    return popup
}