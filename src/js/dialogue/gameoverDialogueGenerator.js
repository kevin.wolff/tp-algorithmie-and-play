import {intervalGeneratorVehicle, intervalScoreUpdate} from '../index.js'

// Generates the game over dialogue.
export default function gameoverDialogueGenerator() {

    // Cleans computer vehicle generator interval.
    clearInterval(intervalGeneratorVehicle)
    clearInterval(intervalScoreUpdate)

    // Cleans computer vehicle still present on board.
    const currentComputerVehicles = document.querySelectorAll('.computer')
    currentComputerVehicles.forEach(vehicle => {
        vehicle.remove()
    })

    const root = document.querySelector('#root')

    const popup = document.createElement('div')
    popup.setAttribute('class', 'popup')
    const msg = document.createElement('p')
    msg.innerText = 'Game over'
    const score = document.createElement('p')
    score.innerText = document.querySelector('.score-value > p').innerHTML
    const btn = document.createElement('div')
    btn.setAttribute('class', 'button')
    btn.innerText = 'Play again'

    btn.addEventListener('click', () => {
        document.location.reload()
    })

    // Appends DOM elements.
    popup.append(msg, score, btn)
    root.append(popup)
}