import constant from './util/constant.js'
import Proxy from './class/Proxy.js'
import Board from './environment/Board.js'
import Score from './environment/Score.js'
import startDialogueGenerator from './dialogue/startDialogueGenerator.js'
import getAvailableVehicles from './function/getAvailableVehicles.js'
import checkLastVehicleDistance from './function/checkLastVehicleDistance.js'
import '../sass/index.scss'

const root = document.querySelector('#root')
const availableVehicles = getAvailableVehicles()
const availableDifficulty = constant.constants.difficulty
let intervalGeneratorVehicle
let intervalScoreUpdate
let currentPlayerScore = 0

// Launcher dialogue.
const startDialogue = startDialogueGenerator(availableVehicles, availableDifficulty)
root.appendChild(startDialogue)

// Launcher dialogue's submit button function.
const submit = document.querySelector('.button')
submit.addEventListener('click', () => {
    let playerVehicleChoice = ''
    let playerDifficultyChoice = ''

    const selectInputs = document.querySelectorAll('.select')
    selectInputs.forEach(select => {
        switch (select.attributes.id.value) {
            case 'selectVehicle':
                playerVehicleChoice = select.value
                break
            case 'selectDifficulty':
                playerDifficultyChoice = parseInt(select.value)
                break
        }
    })

    startDialogue.remove()
    startGame(playerVehicleChoice, playerDifficultyChoice)
})

// Game life cycle.
function startGame(playerVehicleName, playerDifficultyValue) {

    // Generates game board.
    const board = Board.generateBoardInterface(constant.board.width, constant.board.height)

    // Generates game score.
    const score = Score.generateScoreInterface()

    // Start score counting.
    intervalScoreUpdate = setInterval(() => {
        currentPlayerScore = currentPlayerScore + (10 * playerDifficultyValue)
        const scoreValue = document.querySelector('.score-value > p')
        scoreValue.innerHTML = currentPlayerScore
    },1000)

    // Initializes the player's vehicle class.
    const playerVehicle = new Proxy(playerVehicleName, {
        user: 'player',
        constGlobal: constant.constants,
        constVehicle: constant.vehicles[`${playerVehicleName.charAt(0).toLowerCase() + playerVehicleName.slice(1)}`],
        board: board,
        step: null,
        lane: null,
    })

    // Initializes a first class of random vehicle.
    let randomVehicleName = availableVehicles[Math.floor(Math.random() * availableVehicles.length)]
    const randomVehicle = new Proxy(randomVehicleName, {
        user: 'computer',
        constGlobal: constant.constants,
        constVehicle: constant.vehicles[`${randomVehicleName.charAt(0).toLowerCase() + randomVehicleName.slice(1)}`],
        board: board,
        step: playerDifficultyValue,
        lane: Math.floor(Math.random() * 3)
    }).getVehicle()

    // Vehicle creation interval.
    intervalGeneratorVehicle = setInterval(() => {
        if (checkLastVehicleDistance(playerVehicle.constVehicle.height)) {

            // Initializes a class of random vehicle.
            randomVehicleName = availableVehicles[Math.floor(Math.random() * availableVehicles.length)]
            const randomVehicle = new Proxy(randomVehicleName, {
                user: 'computer',
                constGlobal: constant.constants,
                constVehicle: constant.vehicles[`${randomVehicleName.charAt(0).toLowerCase() + randomVehicleName.slice(1)}`],
                board: board,
                step: playerDifficultyValue,
                lane: Math.floor(Math.random() * 2.5)
            }).getVehicle()

            board.appendChild(randomVehicle)
        }
    }, 60)

    // Appends DOM elements.
    board.appendChild(playerVehicle.getVehicle())
    board.appendChild(randomVehicle)
    root.append(score, board)
}

export {intervalGeneratorVehicle, intervalScoreUpdate}