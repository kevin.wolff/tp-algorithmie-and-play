export default class Score {
    constructor() {
    }

    // Generates the score interface.
    static generateScoreInterface() {
        const scoreInterface = document.createElement('div')
        scoreInterface.setAttribute('class', 'score-interface')

        const scoreValue = document.createElement('div')
        scoreValue.setAttribute('class', 'score-value')

        const score = document.createElement('p')
        score.innerHTML = '0'

        scoreValue.append(score)
        scoreInterface.append(scoreValue)

        return scoreInterface
    }
}
