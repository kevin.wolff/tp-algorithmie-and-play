export default class board {
    constructor() {
    }

    // Generates game board interface.
    static generateBoardInterface(width, height) {
        const numLane = 3
        const boardInterface = document.createElement('div')
        boardInterface.setAttribute('class', 'board')
        boardInterface.style.width = `${width}px`
        boardInterface.style.height = `${height}px`
        boardInterface.style.position = 'relative'
        boardInterface.style.display = 'flex'

        for (let i = 0; i < numLane; i++) {
            let lane = document.createElement('div')
            lane.setAttribute('class', `lane lane-${i}`)

            lane.style.width = `${(width / 3)}px`
            lane.style.height = `${height}px`
            boardInterface.append(lane)
        }

        return boardInterface
    }
}