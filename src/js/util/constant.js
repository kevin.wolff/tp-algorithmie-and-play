// Global constants.
exports.constants = {
    keys: ['q', 'd'],
    playerAbscissaStart: 150,
    playerOrdinateStart: 500,
    playerAbscissaLanes: [50, 150, 250],
    computerAbscissaStart: [50, 150, 250],
    computerOrdinateStart: 50,
    difficulty: {slow: 3, normal: 5, fun: 7, intermediate: 10, speed: 20, god: 40}
}

// Board game constants.
exports.board = {
    width: 300,
    height: 550,
}

// Vehicles constants.
exports.vehicles = {
    taxi: {
        type: 'taxi',
        width: 40,
        height: 55,
    },
    car: {
        type: 'car',
        width: 40,
        height: 40,
    },
    truck: {
        type: 'truck',
        width: 40,
        height: 90,
    },
    police: {
        type: 'police',
        width: 50,
        height: 50,
    },
}