import constant from '../util/constant.js'

// Returns all available vehicle present in constant.vehicles.
export default function getAvailableVehicles() {

    const result = []
    const enableVehicles = constant.vehicles
    for (let vehicle in enableVehicles) {
        result.push(vehicle.charAt(0).toUpperCase() + vehicle.slice(1))
    }

    return result
}