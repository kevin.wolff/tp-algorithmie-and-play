// Checks if there are any walls.
export default function isThereWalls(currentVehicle, boardHeight, vehicleHeight) {

    const offsetBottom = parseInt(boardHeight.replace('px', '')) - (currentVehicle.offsetTop + vehicleHeight)
    if (offsetBottom < 20) {
        currentVehicle.remove()
    }

    return false
}