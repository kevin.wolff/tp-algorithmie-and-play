// Check the distance between the next and the last vehicle.
export default function checkLastVehicleDistance(playerVehicleHeight) {

    const currentCars = document.querySelectorAll(`.computer`)
    const closestCar = currentCars[currentCars.length -1]

    return closestCar.offsetTop >= (playerVehicleHeight * 3);
}