// Generates the vehicle DOM element.
export default function generateVehicleDomElement(user, typeVehicle, constVehicle, abscissa, ordinate, lane) {

    const vehicle = document.createElement('div')
    vehicle.setAttribute('class', `${user} ${typeVehicle}`)
    vehicle.dataset.lane = lane
    vehicle.style.position = 'absolute'
    vehicle.style.left = (`${abscissa - (constVehicle.width / 2)}px`)
    vehicle.style.top = (`${ordinate - (constVehicle.height / 2)}px`)
    vehicle.style.width = `${constVehicle.width}px`
    vehicle.style.height = `${constVehicle.height}px`
    vehicle.style.backgroundColor = `${constVehicle.color}`

    return vehicle
}