import gameoverDialogueGenerator from '../dialogue/gameoverDialogueGenerator.js'

// Checks if there are any obstacles.
export default function isThereObstacles(currentVehicle) {

    const playerVehicle = document.querySelector('.player')
    const playerVehicleOffsetTop = playerVehicle.getBoundingClientRect().top
    const playerVehicleCurrentLane = playerVehicle.dataset.lane

    const computerVehicle = currentVehicle
    const computerVehicleOffsetBottom = computerVehicle.getBoundingClientRect().bottom
    const computerVehicleCurrentLane = computerVehicle.dataset.lane

    if ((computerVehicleOffsetBottom - playerVehicleOffsetTop) > 0
        && playerVehicleCurrentLane === computerVehicleCurrentLane) {

        gameoverDialogueGenerator()
        return true
    }

    return false
}