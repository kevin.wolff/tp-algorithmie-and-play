// Checks if there is an available lane.
export default function isThereAvailableLane(currentPlayerAbscissaLane, direction) {

    return direction === 'q' && currentPlayerAbscissaLane > 0 ||
        direction === 'd' && currentPlayerAbscissaLane < 2;
}