const path = require('path');
	const MiniCssExtractPlugin = require('mini-css-extract-plugin');

	module.exports = {
		mode: 'development',
		// mode: 'production',
		entry: {
			index: "./src/js/index.js"
		},
		output: {
			filename: '[name].js',
			path: path.resolve(__dirname, 'public')
		},
	  	plugins: [
		  	new MiniCssExtractPlugin({
		  		filename: '[name].css'
		  	})
	  	],
		module: {
			rules: [
				{
					test: /\.(jpe?g|png)$/,
					type: 'asset/resource',
					generator: { filename: 'asset/[name][ext]' },
				},
				{
					test: /\.css$/,
					use: [
						'style-loader',
						'css-loader'
					]
				},
				{
					test: /\.scss$/,
					use: [
						MiniCssExtractPlugin.loader,
						'css-loader',
						'sass-loader',
					]
				}
			]
		}
	};
